import GPy
import numpy as np
from matplotlib import pyplot as plt
import matplotlib.mlab as ml
import matplotlib.patches as mpatches
import seaborn as sns
import numpy.matlib
import time

dim = 5 # input dataset dimension
#####
##### traing input dataset
#####
tpts = np.array([1,2,3,4,5,6,7],dtype=int) # time points

'''Training input dataset of lowest fidelity'''
training_data = np.loadtxt('growth_neo_hill_100T_added.txt')
xdim1,ydim1 = np.shape(training_data)
X1 = np.zeros((xdim1*7,5))
for ii in range(xdim1):
    X1[7*ii:7*(ii+1),:] = np.hstack([np.matlib.repmat(training_data[ii,:],7,1),tpts[:,None]])

'''Training input dataset of medium fidelity'''
training_data = np.loadtxt('growth_neo_hill_50T_added.txt')
xdim2,ydim2 = np.shape(training_data)
X2 = np.zeros((xdim2*7,5))
for ii in range(xdim2):
    X2[7*ii:7*(ii+1),:] = np.hstack([np.matlib.repmat(training_data[ii,:],7,1),tpts[:,None]])

'''Training input dataset of highest fidelity'''
training_data = np.loadtxt('growth_neo_hill_10T.txt')
xdim3,ydim3 = np.shape(training_data)
X3 = np.zeros((xdim3*7,5))
for ii in range(xdim3):
    X3[7*ii:7*(ii+1),:] = np.hstack([np.matlib.repmat(training_data[ii,:],7,1),tpts[:,None]])

'''Test input dataset
mean of mu, n, k, m, and time_points
50 percentile value = [0.5045, 1.    , 0.549 , 0.04  ]
k_low_med_high = [0.1925,0.549,0.9077] # 10%, 50%, 90%: based on percentile
mu_low_med_high = [0.1080,0.5045,0.8950] # 10%, 50%, 90%: based on percentile
They are calculated with all training input dataset (all fidelities)'''
tpts = np.linspace(1,7,103) # time points
# varying k
Xtest = np.hstack([np.matlib.repmat(np.array([[0.5045,1.,0.1925,0.04]]),103,1),tpts[:,None]])
#Xtest = np.hstack([np.matlib.repmat(np.array([[0.5045,1.,0.5490,0.04]]),103,1),tpts[:,None]])
#Xtest = np.hstack([np.matlib.repmat(np.array([[0.5045,1.,0.9077,0.04]]),103,1),tpts2[:,None]])

# varying mu
#Xtest = np.hstack([np.matlib.repmat(np.array([[0.1080,1.,0.5490,0.04]]),103,1),tpts2[:,None]])
#Xtest = np.hstack([np.matlib.repmat(np.array([[0.5045,1.,0.5490,0.04]]),103,1),tpts2[:,None]])
#Xtest = np.hstack([np.matlib.repmat(np.array([[0.8950,1.,0.5490,0.04]]),103,1),tpts2[:,None]])

#####
##### traing output dataset &
##### do multi-fidelity GP
#####
'''Do GP regression for 100 independently'''
# apex, medium, periphery
# 45th, 34th, 23th -> index becomes 44,33,22
singlepts = np.array([23,34,45],dtype=int)-1  
for igrid in singlepts:

    '''Training output dataset at igridth point of lowest fidelity'''
    Y1 = np.zeros(xdim1*7)
    for ii in range(xdim1):
        Y1_temp = np.loadtxt('f1/job%sthetag.txt'%(ii+1))[:,igrid]
        Y1[7*ii:7*(ii+1)] = Y1_temp
    Y1 = Y1[:,None]
    
    '''Training output dataset at igridth point of medium fidelity'''
    Y2 = np.zeros(xdim2*7)
    for ii in range(xdim2):
        Y2_temp = np.loadtxt('f2/job%sthetag.txt'%(ii+1))[:,igrid]
        Y2[7*ii:7*(ii+1)] = Y2_temp
    Y2 = Y2[:,None]

    '''Training output dataset at igridth point of highest fidelity'''
    Y3 = np.zeros(xdim3*7)
    for ii in range(xdim3):
        Y3_temp = np.loadtxt('f3/job%sthetag.txt'%(ii+1))[:,igrid]
        Y3[7*ii:7*(ii+1)] = Y3_temp
    Y3 = Y3[:,None]

    active_dimensions = np.arange(0,dim)

    start = time.time()

    ''' Train level 1 '''
    k1 = GPy.kern.RBF(dim, ARD=True)
    m1 = GPy.models.GPRegression(X=X1, Y=Y1, kernel=k1)
    m1[".*Gaussian_noise"] = m1.Y.var()*0.01
    m1[".*Gaussian_noise"].fix()

    m1.optimize(max_iters = 500)
    print('*' * 80)
    print('Optimized model1:')
    print(m1)

    m1[".*Gaussian_noise"].unfix()
    m1[".*Gaussian_noise"].constrain_positive()
 
    Xp = X2
    mu1, v1 = m1.predict(Xp)
    print('Finish train level 1')

    ''' Train level 2 '''
    XX2 = np.hstack((X2, mu1))

    k2 = GPy.kern.RBF(1, active_dims = [dim])*GPy.kern.RBF(dim, active_dims = active_dimensions, ARD = True) \
        + GPy.kern.RBF(dim, active_dims = active_dimensions, ARD = True)

    m2 = GPy.models.GPRegression(X=XX2, Y=Y2, kernel=k2)
    m2[".*Gaussian_noise"] = m2.Y.var()*0.01
    m2[".*Gaussian_noise"].fix()

    m2.optimize(max_iters = 500)
    print('*' * 80)
    print('Optimized model2:')
    print(m2)


    m2[".*Gaussian_noise"].unfix()
    m2[".*Gaussian_noise"].constrain_positive()

    print('Finish train level 2')

    # Prepare for level 3: sample f_1 at X3
    nsamples = 100
    ntest = X3.shape[0]
    mu0, C0 = m1.predict(X3, full_cov=True)
    Z = np.random.multivariate_normal(mu0.flatten(),C0,nsamples)
    tmp_m = np.zeros((nsamples,ntest))
    tmp_v = np.zeros((nsamples,ntest))

    # push samples through f_2
    for i in range(0,nsamples):
        mu, v = m2.predict(np.hstack((X3, Z[i,:][:,None])))
        tmp_m[i,:] = mu.flatten()
        tmp_v[i,:] = v.flatten()

    # get mean and variance at X3
    mu2 = np.mean(tmp_m, axis = 0)
    v2 = np.mean(tmp_v, axis = 0) + np.var(tmp_m, axis = 0)
    mu2 = mu2[:,None]
    v3 = np.abs(v2[:,None])


    ''' Train level 3'''
    XX3 = np.hstack((X3, mu2))
    
    k3 = GPy.kern.RBF(1, active_dims = [dim])*GPy.kern.RBF(dim, active_dims = active_dimensions, ARD = True) \
        + GPy.kern.RBF(dim, active_dims = active_dimensions, ARD = True)

    m3 = GPy.models.GPRegression(X=XX3, Y=Y3, kernel=k3)
    
    m3[".*Gaussian_noise"] = m3.Y.var()*0.01
    m3[".*Gaussian_noise"].fix()

    m3.optimize(max_iters = 500)
    print('*' * 80)
    print('Optimized model3:')
    print(m3)

    m3[".*Gaussian_noise"].unfix()
    m3[".*Gaussian_noise"].constrain_positive()

    print('Finish train level 3')

    end = time.time()

    print("Training AR-GP done in %f seconds" % (end - start))

    ##########################
    ##########################
    # Predict at test points #
    ########################## 
    ##########################

    ntest = Xtest.shape[0]
    mu0, C0 = m1.predict(Xtest, full_cov=True)
    Z = np.random.multivariate_normal(mu0.flatten(),C0,nsamples)
    tmp_m = np.zeros((nsamples**2,ntest))
    tmp_v = np.zeros((nsamples**2,ntest))

    cnt = 0
    for i in range(0,nsamples):
        mu, C = m2.predict(np.hstack((Xtest, Z[i,:][:,None])), full_cov=True)
        Q = np.random.multivariate_normal(mu.flatten(),C,nsamples)
        for j in range(0,nsamples):
            mu, v = m3.predict(np.hstack((Xtest, Q[j,:][:,None])))
            tmp_m[cnt,:] = mu.flatten()
            tmp_v[cnt,:] = v.flatten()
            cnt += 1

    mu3 = np.mean(tmp_m, axis = 0)
    v3 = np.mean(tmp_v, axis = 0) + np.var(tmp_m, axis = 0)
    mu3 = mu3[:,None]
    v3 = np.abs(v3[:,None])

    np.savetxt('thetag_mean_%spt.txt'%(igrid+1),mu3,fmt='%.6f')
    np.savetxt('thetag_variance_%spt.txt'%(igrid+1),v3,fmt='%.10f')

    print('%sth grid point for medium k - prediction done'%(igrid+1))
